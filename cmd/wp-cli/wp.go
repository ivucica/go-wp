package main

import (
	"badc0de.net/pkg/flagutil"
	"context"
	"flag"
	"github.com/golang/glog"
	"github.com/ssttevee/go-wordpress"
)

var (
	dbHost = flag.String("db_host", "", "hostname")
	dbUser = flag.String("db_user", "", "username")
	dbPass = flag.String("db_pass", "", "password")
	dbName = flag.String("db_name", "", "database")
	dbPrfx = flag.String("db_prfx", "", "dbprefix")
	wpBase = flag.String("wp_base", "", "base url")
)

func main() {
	flagutil.Parse()
	glog.Infof("connecting")
	wp, err := wordpress.New(*dbHost, *dbUser, *dbPass, *dbName)
	if err != nil {
		glog.Fatalf("probl: %v", err)
	}
	defer wp.Close()
	wp.TablePrefix = *dbPrfx
	wp.SetMaxOpenConns(3)
	ctx := context.Background()
	ctx = wordpress.NewContext(ctx, wp)

	opts := &wordpress.ObjectQueryOptions{
		Limit: 9999,
	}
	posts, err := wordpress.QueryPosts(ctx, opts)
	if err != nil {
		glog.Fatalf("probl: %v", err)
	}
	for posts != nil {
		slice, err := posts.Slice()
		if err != nil {
			glog.Fatalf("probl: %v", err)
		}
		glog.Infof("slice: %v", slice)

		postsF(ctx, slice)

		num, err := posts.Next()
		if err != nil {
			if err == wordpress.Done {
				break
			}
			glog.Fatalf("probl: %v", err)
		} else {
			glog.Infof("num: %d", num)
		}
	}
}

func postsF(ctx context.Context, slice []int64) error {
	posts, err := wordpress.GetPosts(ctx, slice...)
	if err != nil {
		glog.Errorf("err: %v", err)
		return err
	}
	for _, post := range posts {
		glog.Infof("* %v", post.Name)
		glog.Infof("  * %v", post.Title)
		glog.Infof("  * %s/%s/%s/%s.html", *wpBase, post.Date.Format("2006"), post.Date.Format("01"), post.Name)
		glog.Infof("  * id: %s/?p=%d", *wpBase, post.Id)
		glog.Infof("  * status: %v", post.Status)
	}
	return nil
}
