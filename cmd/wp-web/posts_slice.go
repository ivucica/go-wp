package main

import (
	"context"
	"net/http"

	"github.com/golang/glog"
	"github.com/ssttevee/go-wordpress"
)

func (web *WPWeb) postsSlice(w http.ResponseWriter, r *http.Request, wpctx context.Context, slice []int64, detailed bool) {
	posts, err := wordpress.GetPosts(wpctx, slice...)
	if err != nil {
		glog.Errorf("wpweb.posts: err: %v", err)
		return
	}

	posted := false
	for idx, post := range posts {
		if post.Status != wordpress.PostStatusPublish && post.Status != "" {
			glog.Infof("wpweb.posts: post %d has status %s, skipping", post.Id, post.Status)
			continue
		}
		p := web.formatPost(wpctx, idx, post)
		p.write(wpctx, w, r, web)
		posted = true
	}
	if !posted {
		http.Error(w, "Not found. Sorry.", http.StatusNotFound)
	}
}
