package main

import (
	"context"
	"net/http"
	"os"
	"strings"

	"github.com/ssttevee/go-wordpress"
)

func (web *WPWeb) page(w http.ResponseWriter, r *http.Request) error {
	// Check if it's a page.
	// TODO: parsing the name is wrong
	wpctx := context.Background()
	wpctx = wordpress.NewContext(r.Context(), web.db)

	pageName := r.URL.Path[1:]
	if strings.HasSuffix(pageName, "/amp") {
		pageName = pageName[:len(pageName)-len("/amp")]
		wpctx = context.WithValue(wpctx, "go-wp:serve-amp", true)
	}

	if pagesIt, err := wordpress.QueryPosts(wpctx, &wordpress.ObjectQueryOptions{PostType: wordpress.PostTypePage, Name: pageName, PostStatus: wordpress.PostStatusPublish}); err == nil {
		pages, err := pagesIt.Slice()
		if err == nil && len(pages) > 0 {
			// Recreate the iterator to reset it after being consumed in Slice.
			// TODO: move to web.posts or web.post which also accepts a slice of ints.
			if pagesIt, err := wordpress.QueryPosts(wpctx, &wordpress.ObjectQueryOptions{PostType: wordpress.PostTypePage, Name: pageName}); err == nil {
				wpctx = context.WithValue(wpctx, "go-wp:single-post-mode", true)
				web.posts(w, r, wpctx, pagesIt, false, 0 /* skip nothing */, 1 /* there will be only one page of posts */)
				return nil
			}
		}
	}
	return os.ErrNotExist
}
