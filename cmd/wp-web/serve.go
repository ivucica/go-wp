package main

import (
	"bytes"
	"context"
	"io"
	"strings"

	"github.com/dustin/go-humanize"
	"github.com/golang/glog"
	"github.com/gorilla/mux"
	"github.com/ssttevee/go-wordpress"
	"golang.org/x/net/html"

	"html/template"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
)

type WPWeb struct {
	db            *wordpress.WordPress
	templates     *template.Template
	templatesFeed *template.Template
	templatesAMP  *template.Template
	rtr           *mux.Router

	tmplFeedModTime time.Time

	BaseURL       *url.URL
	WPContentPath string
	PageSize      int
	SiteID        int
	GAAccount     string

	shortcodeFuncMap ShortcodeFuncMap

	RobotsAllowed bool
}

func joinURL(u *url.URL, path string) *url.URL {
	u2 := *u
	u2.Path += path
	u2.RawPath += path
	return &u2
}

// Temporary function. This should be replaced by New() function. Instead this depends on WPWeb being populated already.
func (web *WPWeb) tempfunc__Init() {
	var err error

	web.RegisterShortcode("contact-form", func(ctx context.Context, attr map[string][]string, content []byte) ([]byte, error) {
		return []byte("(Contact form temporarily not supported.)"), nil
	})
	web.RegisterShortcode("portfolio", func(ctx context.Context, attr map[string][]string, content []byte) ([]byte, error) {
		// https://wordpress.com/support/portfolios/portfolio-shortcode/
		return []byte("(Portfolio rendering temporarily not supported.)"), nil
	})
	web.RegisterShortcode("caption", func(ctx context.Context, attr map[string][]string, content []byte) ([]byte, error) {
		// https://codex.wordpress.org/Caption_Shortcode
		// [caption id=“attachment_5690” align=“aligncenter” width=“300”]<img src="..."> caption here[/caption]
		// or
		// ]<a href="..."><img src="..."></a> caption here</a>[/caption]

		var imgAccumulator, textAccumulator []byte
		z := html.NewTokenizer(bytes.NewReader(content))
	loop:
		for {
			tt := z.Next()
			switch tt {
			case html.ErrorToken:
				// Returning io.EOF indicates success.
				if z.Err() != io.EOF {
					glog.Errorf("error tokenizing HTML: %v", z.Err())
				}
				break loop
			case html.TextToken:
				textAccumulator = append(textAccumulator, z.Text()...)
			case html.StartTagToken:
				imgAccumulator = append(imgAccumulator, z.Raw()...)
			case html.EndTagToken:
				imgAccumulator = append(imgAccumulator, z.Raw()...)
			case html.SelfClosingTagToken, html.DoctypeToken, html.CommentToken:
				imgAccumulator = append(imgAccumulator, z.Raw()...)
			default:
				glog.Warningf("unknown html token type seen, breaking: %v", tt)
				break loop
			}

		}
		buf := bytes.Buffer{}
		buf.Write([]byte("<div class='_temp_captioned_img' style='border: 1px #ccc solid;'><div class='_temp_img'>"))
		buf.Write(imgAccumulator)
		buf.Write([]byte("</div><div class='_temp_caption' style='font-size: 0.7em;'>"))
		buf.Write(textAccumulator)
		buf.Write([]byte("</div></div>"))
		return buf.Bytes(), nil
	})

	funcs := template.FuncMap{}
	funcs["time_rfc3339"] = func(tm time.Time) string {
		return tm.Format(time.RFC3339)
	}
	funcs["time_humanize"] = func(tm time.Time) string {
		return humanize.Time(tm)
	}
	funcs["time_short"] = func(tm time.Time) string {
		return tm.Format(time.RFC822Z)
	}
	funcs["cdata_str"] = func(txt string) template.HTML {
		txtEscaped := strings.Replace(txt, `]]>`, `]]]><![CDATA[>`, -1) // https://stackoverflow.com/a/223782/39974
		return template.HTML(`<![CDATA[` + txtEscaped + `]]>`)
	}
	funcs["cdata"] = func(txt template.HTML) template.HTML {
		txtEscaped := strings.Replace(string(txt), `]]>`, `]]]><![CDATA[>`, -1) // https://stackoverflow.com/a/223782/39974
		return template.HTML(`<![CDATA[` + txtEscaped + `]]>`)
	}
	funcs["xml_prolog"] = func() template.HTML {
		return template.HTML(`<?xml version="1.0" encoding="UTF-8"?>`)
	}
	funcs["menu"] = func(wpctx context.Context, name string) *menu {
		m, err := web.menu(wpctx, name)
		if err != nil {
			glog.Errorf("wpweb.funcs.menu: %v", err)
			return &menu{}
		}
		return m
	}
	funcs["menu_item"] = func(wpctx context.Context, mi *wordpress.MenuItem) struct {
		Context  context.Context
		Web      *WPWeb
		MenuItem *wordpress.MenuItem
	} {
		return struct {
			Context  context.Context
			Web      *WPWeb
			MenuItem *wordpress.MenuItem
		}{
			wpctx, web, mi,
		}
	}

	tmpl := template.New("go-wp")
	tmpl = tmpl.Funcs(funcs)
	tmpl, err = tmpl.ParseFiles("templates/index.html", "templates/entry.html")
	if err != nil {
		glog.Errorf("wpweb.posts: tmpl parse err: %v", err)
		return
	}
	web.templates = tmpl

	tmpl = template.New("go-wp-amp")
	tmpl = tmpl.Funcs(funcs)
	tmpl, err = tmpl.ParseFiles("templates/index-amp.html")
	if err != nil {
		glog.Errorf("wpweb.posts: tmpl-amp parse err: %v", err)
		return
	}
	web.templatesAMP = tmpl

	tmpl = template.New("go-wp-feed")
	tmpl = tmpl.Funcs(funcs)
	tmpl, err = tmpl.ParseFiles("templates/index-feed.xml") //, "templates/entry-feed.xml")
	if err != nil {
		glog.Errorf("wpweb.posts: tmpl-feed parse err: %v", err)
		return
	}
	web.templatesFeed = tmpl

	fi, err := os.Stat("templates/index-feed.xml")
	if err != nil {
		glog.Errorf("wpweb.posts: tmpl-feed stat err: %v", err)
		return
	}
	web.tmplFeedModTime = fi.ModTime()

	baseU := web.BaseURL
	rtr := mux.NewRouter()
	rtr = rtr.Host(baseU.Host).PathPrefix(baseU.Path).Subrouter()

	rtr.HandleFunc("/robots.txt", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Cache-Control", "max-age=1800, max-stale=1800, no-cache, no-store, must-revalidate")
		w.Header().Add("Expires", time.Now().Add(1800*time.Second).Format(http.TimeFormat))
		if !web.RobotsAllowed {
			w.Write([]byte(`User-Agent: *
Disallow: /
`))
			return
		}
		w.Write([]byte(`User-Agent: *
Allow: /

Sitemap: ` + web.BaseURL.String() + `/sitemap.xml
`))
	})

	rtr.HandleFunc("/{year:[0-9]+}/{month:[0-9]+}/{name}.html", func(w http.ResponseWriter, r *http.Request) {
		wpctx := wordpress.NewContext(r.Context(), web.db)
		web.postPage(wpctx, w, r)
	})
	rtr.HandleFunc("/{year:[0-9]+}/{month:[0-9]+}/{name}.html/feed", func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Feed not implemented", http.StatusNotImplemented)
	})
	rtr.HandleFunc("/{year:[0-9]+}/{month:[0-9]+}/{name}.html/amp", func(w http.ResponseWriter, r *http.Request) {
		wpctx := wordpress.NewContext(r.Context(), web.db)
		wpctx = context.WithValue(wpctx, "go-wp:serve-amp", true)
		web.postPage(wpctx, w, r)
	})
	rtr.HandleFunc("/{year:[0-9]+}/{month:[0-9]+}/{name}.html/{attachment:.+}", func(w http.ResponseWriter, r *http.Request) {
		glog.Infof("post %q attachment %q requested", mux.Vars(r)["name"], mux.Vars(r)["attachment"])
		wpctx := wordpress.NewContext(r.Context(), web.db)
		web.postPage(wpctx, w, r)
	})
	rtr.HandleFunc("/author/{author}", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		author := v["author"]
		web.home(w, r, 1, wordpress.ObjectQueryOptions{AuthorName: author}, false, false)
	})
	rtr.HandleFunc("/author/{author}/feed", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		author := v["author"]
		web.home(w, r, 1, wordpress.ObjectQueryOptions{AuthorName: author}, true, false)
	})
	rtr.HandleFunc("/author/{username}/page/{page:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		author := v["author"]
		pg, _ := strconv.Atoi(v["page"])
		if pg > 0 {
			web.home(w, r, pg, wordpress.ObjectQueryOptions{AuthorName: author}, false, false)
		} else {
			glog.Errorf("non-positive page passed: %d", pg)
			http.Error(w, "Not found", http.StatusNotFound)
		}
	})

	rtr.HandleFunc("/page/{page:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		pg, _ := strconv.Atoi(v["page"])
		if pg > 0 {
			web.home(w, r, pg, wordpress.ObjectQueryOptions{}, false, false)
		} else {
			glog.Errorf("non-positive page passed: %d", pg)
			http.Error(w, "Not found", http.StatusNotFound)
		}
	})

	// Note: ok with tags such as 'gnu/linux', as the tag will be normalized already to 'gnulinux'.
	rtr.HandleFunc("/tag/{tag}/page/{page:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		tag := v["tag"]
		pg, _ := strconv.Atoi(v["page"])
		if pg > 0 {
			web.home(w, r, pg, wordpress.ObjectQueryOptions{TagName: tag}, false, false)
		} else {
			glog.Errorf("non-positive page passed: %d", pg)
			http.Error(w, "Not found", http.StatusNotFound)
		}
	})
	// Note: ok with tags such as 'gnu/linux', as the tag will be normalized already to 'gnulinux'.
	rtr.HandleFunc("/tag/{tag}", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		tag := v["tag"]
		web.home(w, r, 1, wordpress.ObjectQueryOptions{TagName: tag}, false, false)
	})
	rtr.HandleFunc("/tag/{tag}/feed", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		tag := v["tag"]
		web.home(w, r, 1, wordpress.ObjectQueryOptions{TagName: tag}, true, false)
	})

	rtr.HandleFunc("/category/{category:.+}/page/{page:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		category := v["category"]
		pg, _ := strconv.Atoi(v["page"])
		if pg > 0 {
			web.home(w, r, pg, wordpress.ObjectQueryOptions{CategoryName: category}, false, false)
		} else {
			glog.Errorf("non-positive page passed: %d", pg)
			http.Error(w, "Not found", http.StatusNotFound)
		}
	})
	rtr.HandleFunc("/category/{category:.+}/feed", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		category := v["category"]
		web.home(w, r, 1, wordpress.ObjectQueryOptions{CategoryName: category}, true, false)
	})
	// Must be defined last, lest it captures feed and page.
	rtr.HandleFunc("/category/{category:.+}", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		category := v["category"]
		web.home(w, r, 1, wordpress.ObjectQueryOptions{CategoryName: category}, false, false)
	})

	rtr.Handle("/wp-content/plugins/jquery-syntax/jquery-syntax/{file}", http.StripPrefix(web.BaseURL.Path+"/wp-content/plugins/jquery-syntax/jquery-syntax", http.FileServer(http.Dir(web.WPContentPath+"/plugins/jquery-syntax/jquery-syntax"))))
	rtr.Handle("/wp-content/uploads/{year}/{month}/{file}", http.StripPrefix(web.BaseURL.Path+"/wp-content/uploads", http.FileServer(http.Dir(web.WPContentPath+"/uploads"))))
	rtr.Handle("/images/{file}", http.StripPrefix(web.BaseURL.Path+"/images", http.FileServer(http.Dir("templates/images"))))
	rtr.HandleFunc("/styles.css", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "templates/styles.css")
	})

	rtr.HandleFunc("/{year:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		year, err := strconv.Atoi(v["year"])
		if err != nil {
			glog.Errorf("year conversion failed: %v", err)
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
			return
		}
		web.home(w, r, 1, wordpress.ObjectQueryOptions{Year: year}, false, false)
	})
	rtr.HandleFunc("/{year:[0-9]+}/page/{page:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		year, err := strconv.Atoi(v["year"])
		if err != nil {
			glog.Errorf("year conversion failed: %v", err)
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
			return
		}
		pg, err := strconv.Atoi(v["page"])
		if err != nil {
			glog.Errorf("page conversion failed: %v", err)
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
			return
		}
		if pg > 0 {
			web.home(w, r, pg, wordpress.ObjectQueryOptions{Year: year}, false, false)
		} else {
			glog.Errorf("non-positive page %d passed", pg)
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
		}
	})
	// TODO: support yearly feed.

	rtr.HandleFunc("/{year:[0-9]+}/{month:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		year, err := strconv.Atoi(v["year"])
		if err != nil {
			glog.Errorf("year conversion failed: %v", err)
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
			return
		}
		month, err := strconv.Atoi(v["month"])
		if err != nil {
			glog.Errorf("year conversion failed: %v", err)
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
			return
		}
		web.home(w, r, 1, wordpress.ObjectQueryOptions{Year: year, Month: month}, false, false)
	})
	rtr.HandleFunc("/{year:[0-9]+}/page/{page:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		year, err := strconv.Atoi(v["year"])
		if err != nil {
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
			return
		}
		month, err := strconv.Atoi(v["month"])
		if err != nil {
			glog.Errorf("month conversion failed: %v", err)
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
			return
		}
		pg, err := strconv.Atoi(v["page"])
		if err != nil {
			glog.Errorf("page conversion failed: %v", err)
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
			return
		}
		if pg > 0 {
			web.home(w, r, pg, wordpress.ObjectQueryOptions{Year: year, Month: month}, false, false)
		} else {
			glog.Errorf("non-positive page %d passed", pg)
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
		}
	})
	// TODO: support monthly feed.

	rtr.HandleFunc("/sitemap.xml", func(w http.ResponseWriter, r *http.Request) {
		wpctx := wordpress.NewContext(r.Context(), web.db)
		web.SitemapIndex(wpctx, w, r)
	})
	rtr.HandleFunc("/sitemap-misc.xml", func(w http.ResponseWriter, r *http.Request) {
		wpctx := wordpress.NewContext(r.Context(), web.db)
		web.SitemapMisc(wpctx, w, r)
	})
	rtr.HandleFunc("/sitemap-authors.xml", func(w http.ResponseWriter, r *http.Request) {
		wpctx := wordpress.NewContext(r.Context(), web.db)
		web.SitemapAuthors(wpctx, w, r)
	})
	rtr.HandleFunc("/sitemap-tax-{taxonomy:(?:post_tag|category)}.xml", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		taxonomy := v["taxonomy"]
		wpctx := wordpress.NewContext(r.Context(), web.db)
		web.SitemapTaxonomy(wpctx, w, r, taxonomy)
	})
	rtr.HandleFunc("/sitemap-pt-{posttype:(?:post|page)}-{year:[0-9]+}-{month:[0-9]+}.xml", func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		year, err := strconv.Atoi(v["year"])
		if err != nil {
			glog.Errorf("year conversion failed: %v", err)
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
			return
		}
		month, err := strconv.Atoi(v["month"])
		if err != nil {
			glog.Errorf("month conversion failed: %v", err)
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
			return
		}
		pt := wordpress.PostType(v["posttype"])
		wpctx := wordpress.NewContext(r.Context(), web.db)
		web.SitemapPostType(wpctx, w, r, pt, year, month)
	})

	rtr.HandleFunc("/amp", func(w http.ResponseWriter, r *http.Request) {
		web.home(w, r, 1, wordpress.ObjectQueryOptions{}, false, true)
	})
	rtr.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		wpctx := wordpress.NewContext(r.Context(), web.db)
		if done := web.jetpack(wpctx, w, r); done {
			return
		}
		web.home(w, r, 1, wordpress.ObjectQueryOptions{}, false, false)
	})
	rtr.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if err := web.page(w, r); err != nil {
			glog.Errorf("unhandled page requested")
			http.Error(w, "Not found. Sorry.", http.StatusNotFound)
		}
	})

	web.rtr = rtr
}

func (web *WPWeb) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	web.rtr.ServeHTTP(w, r)
}
