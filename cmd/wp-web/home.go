package main

import (
	"context"
	"net/http"
	"strconv"

	"github.com/golang/glog"
	"github.com/ssttevee/go-wordpress"
)

func (web *WPWeb) home(w http.ResponseWriter, r *http.Request, page int, opts wordpress.ObjectQueryOptions, feed, amp bool) {
	wpctx := r.Context()
	wpctx = wordpress.NewContext(wpctx, web.db)
	if feed {
		// TODO: use a custom type for value key, per documentation
		wpctx = context.WithValue(wpctx, "go-wp:serve-feed", true)
	}
	if amp {
		// TODO: use a custom type for value key, per documentation
		wpctx = context.WithValue(wpctx, "go-wp:serve-amp", true)
	}

	postIDs, ok := r.URL.Query()["p"]
	if ok && len(postIDs) > 0 {
		postID, err := strconv.Atoi(postIDs[0])
		if err == nil {
			web.postsPage(w, r, wpctx, []int64{int64(postID)})
			return
		}
	}

	opts.PostStatus = wordpress.PostStatusPublish
	opts.Limit = web.PageSize * page // No support for skipping!?
	opts.Order = "post_date"

	if opts.CategoryName != "" {
		wpctx = context.WithValue(wpctx, "go-wp:category:slug", opts.CategoryName)
		categoryID, err := wordpress.GetCategoryIdBySlug(wpctx, opts.CategoryName)
		if err != nil {
			glog.Errorf("could not find category id for category %q: %v", opts.CategoryName, err)
		} else {
			wpctx = context.WithValue(wpctx, "go-wp:category:id", categoryID)
			categories, err := wordpress.GetCategories(wpctx, categoryID)
			if err != nil {
				glog.Errorf("could not find category for category %q (id %d): %v", opts.CategoryName, categoryID, err)
			} else {
				if len(categories) == 0 {
					glog.Errorf("could not find category for category %q (id %d): no categories returned", opts.CategoryName, categoryID)
				} else {
					wpctx = context.WithValue(wpctx, "go-wp:category:data", categories[0])
				}
			}
		}
	}

	if opts.TagName != "" {
		wpctx = context.WithValue(wpctx, "go-wp:tag:slug", opts.TagName)
		tagID, err := wordpress.GetTagIdBySlug(wpctx, opts.TagName)
		if err != nil {
			glog.Errorf("could not find tag id for tag %q: %v", opts.TagName, err)
		} else {
			wpctx = context.WithValue(wpctx, "go-wp:tag:id", tagID)
			tags, err := wordpress.GetTags(wpctx, tagID)
			if err != nil {
				glog.Errorf("could not find tag for tag %q (id %d): %v", opts.TagName, tagID, err)
			} else {
				if len(tags) == 0 {
					glog.Errorf("could not find tag for tag %q (id %d): no tags returned", opts.TagName, tagID)
				} else {
					wpctx = context.WithValue(wpctx, "go-wp:tag:data", tags[0])
				}
			}
		}
	}

	posts, err := wordpress.QueryPosts(wpctx, &opts)
	if err != nil {
		glog.Infof("probl: %v", err)
		http.Error(w, "Not found", http.StatusNotFound)
		return

	}

	// Hacky skipping.
	hackyPleaseSkip := (page - 1) * web.PageSize
	glog.Info("Skipping ", hackyPleaseSkip)

	// TODO(ivucica): nav page will be wrong when tag is !nil.
	web.posts(w, r, wpctx, posts, false, hackyPleaseSkip, page+1)
}
