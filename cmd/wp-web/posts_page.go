package main

import (
	"context"
	"html/template"
	"net/http"
	"net/url"
	"time"

	"github.com/golang/glog"
	"github.com/ssttevee/go-wordpress"
)

func (web *WPWeb) postsPage(w http.ResponseWriter, r *http.Request, wpctx context.Context, posts []int64) {
	var err error

	tmpl := web.templates
	if v := wpctx.Value("go-wp:serve-feed"); v != nil {
		tmpl = web.templatesFeed
	}
	if v := wpctx.Value("go-wp:serve-amp"); v != nil {
		tmpl = web.templatesAMP
	}

	blogName, err := wordpress.GetOption(wpctx, "blogname")
	if err != nil {
		glog.Errorf("failed to read blogname option: %v", err)
		blogName = "go-wp"
	}

	blogDescription, err := wordpress.GetOption(wpctx, "blogdescription")
	if err != nil {
		glog.Errorf("failed to read blog description: %v", err)
		blogDescription = ""
	}

	category, ok := wpctx.Value("go-wp:category:data").(*wordpress.Category)
	if !ok {
		category = nil
	}

	tag, ok := wpctx.Value("go-wp:tag:data").(*wordpress.Tag)
	if !ok {
		tag = nil
	}

	listingHeaderData := struct {
		Title        string
		MDLBaseURL   string
		BaseURL      *url.URL
		SiteID       int
		GenTime      time.Time
		GAAccount    string
		CanonicalURL *url.URL
		AMPURL       *url.URL
		Category     *wordpress.Category
		Tag          *wordpress.Tag
		SiteTagline  template.HTML // TODO: for some reason, the description is stored escaped, which seems bad; use bluemonday policy?
		Context      context.Context
	}{
		Title:       blogName,
		MDLBaseURL:  "https://code.getmdl.io/1.3.0",
		BaseURL:     web.BaseURL,
		SiteID:      web.SiteID,
		GenTime:     web.tmplFeedModTime,
		GAAccount:   web.GAAccount,
		SiteTagline: template.HTML(blogDescription),
		Category:    category,
		Tag:         tag,
		Context:     wpctx,
	}

	postData, err := wordpress.GetPosts(wpctx, posts...)
	if err != nil {
		glog.Errorf("wpweb.postsPage: err: %v", err)
		return
	}
	posted := false
	var singlePost *wordpress.Post
	for _, post := range postData {
		if post.Status != wordpress.PostStatusPublish && post.Status != "" {
			glog.Infof("wpweb.posts: post %d has status %s, skipping", post.Id, post.Status)
			continue
		}
		if !posted {
			singlePost = post
		} else {
			singlePost = nil
		}

		posted = true
		if post.ModifiedGmt.After(listingHeaderData.GenTime) {
			listingHeaderData.GenTime = post.ModifiedGmt
		}
	}
	if !posted {
		http.Error(w, "Not found. Sorry.", http.StatusNotFound)
		return
	}

	if singlePostRendering, ok := wpctx.Value("go-wp:single-post-mode").(bool); singlePost != nil && singlePostRendering && ok {
		listingHeaderData.CanonicalURL = canonicalPostURL(singlePost)
		listingHeaderData.AMPURL = ampPostURL(singlePost)
		// TODO: add canonical URL for home page, archive pages, etc
	}

	err = tmpl.ExecuteTemplate(w, "listing_header", listingHeaderData)
	if err != nil {
		glog.Errorf("failed to execute listing_header: %v", err)
		return
	}

	////////////////////////

	for idx, post := range postData {
		if post.Status != wordpress.PostStatusPublish && post.Status != "" {
			continue
		}
		p := web.formatPost(wpctx, idx, post)
		p.write(wpctx, w, r, web)
	}

	///////////////////////

	err = tmpl.ExecuteTemplate(w, "listing_nav", struct {
		Cols        int
		ColsDesktop int
		NextPage    int
		BaseURL     *url.URL
	}{
		Cols:        12,
		ColsDesktop: 12,
		NextPage:    0, // page + 1, // TODO(ivucica): remove next page or maybe listing_nav
		BaseURL:     web.BaseURL,
	})
	if err != nil {
		glog.Errorf("failed to execute listing_nav: %v", err)
		return
	}

	err = tmpl.ExecuteTemplate(w, "listing_footer", struct {
		MDLBaseURL string
		BaseURL    *url.URL
	}{
		MDLBaseURL: "https://code.getmdl.io/1.3.0",
		BaseURL:    web.BaseURL,
	})
	if err != nil {
		glog.Errorf("failed to execute listing_footer: %v", err)
		return
	}
}
