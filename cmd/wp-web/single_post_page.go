package main

import (
	"context"
	"github.com/golang/glog"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/ssttevee/go-wordpress"
)

func (web *WPWeb) postPage(wpctx context.Context, w http.ResponseWriter, r *http.Request) {
	postName := mux.Vars(r)["name"]
	postYearS := mux.Vars(r)["year"]
	postMonthS := mux.Vars(r)["month"]

	postYear, err := strconv.ParseInt(postYearS, 10, 64)
	if err != nil {
		glog.Warningf("passed bad year: %s: %v", postYearS, err)
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	postMonth, err := strconv.ParseInt(postMonthS, 10, 64)
	if err != nil {
		glog.Warningf("passed bad month: %s: %v", postYearS, err)
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	posts, err := wordpress.QueryPosts(wpctx, &wordpress.ObjectQueryOptions{
		PostStatus: wordpress.PostStatusPublish,
		Name:       postName,
		Year:       int(postYear),
		Month:      int(postMonth),
	})
	if err != nil {
		glog.Errorf("probl: %v", err)
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}

	if attachmentSlug, wantsAttachment := mux.Vars(r)["attachment"]; wantsAttachment {
		postID, err := posts.Next()
		if err != nil {
			glog.Errorf("attachment %q probl: cannot find post id for %q: %v", attachmentSlug, postName, err)
			http.Error(w, "Not found", http.StatusNotFound)
			return
		}
		parentPostA, err := wordpress.GetPosts(wpctx, postID)
		if err != nil || len(parentPostA) == 0 {
			glog.Errorf("attachment %q probl: cannot get post %q (%d): %v", attachmentSlug, postName, postID, err)
			http.Error(w, "Not found", http.StatusNotFound)
			return
		}
		if parentPostA[0].Status != wordpress.PostStatusPublish && parentPostA[0].Status != "" { // TODO(ivucica): why 5732 under post 5724 is giving empty string for 5724?
			// we already sort of know this is going to be the case; the check is superfluous, but guards against changes to wordpress.QueryPosts.
			// this is needed, because next up we have to check 'Inherit'.
			glog.Errorf("parent %q not published: %q", parentPostA[0].Title, parentPostA[0].Status)
			http.Error(w, "Not found", http.StatusNotFound)
			return
		}

		if parentPostA[0].Status == "" {
			glog.Warningf("parent post status in database unlikely to be empty string; check relationship between child %q under parent %d", attachmentSlug, postID)
		}

		posts, err = wordpress.QueryPosts(wpctx, &wordpress.ObjectQueryOptions{
			Name:       attachmentSlug,
			Parent:     postID,
			PostType:   wordpress.PostTypeAttachment,
			PostStatus: wordpress.PostStatusInherit,
		})
		if err != nil {
			glog.Errorf("attachment %q probl: cannot get subpost of %q (%d): %v", attachmentSlug, postName, postID, err)
			http.Error(w, "Not found", http.StatusNotFound)
			return
		}
		glog.Infof("searching for attachment %q under post %d", attachmentSlug, postID)
		wpctx = context.WithValue(wpctx, "go-wp:post-parent", postID)

		// TODO(ivucica): support rendering media, such as 5732 under post 5724

	}

	wpctx = context.WithValue(wpctx, "go-wp:single-post-mode", true)

	web.posts(w, r, wpctx, posts, true, 0, -1)
}
