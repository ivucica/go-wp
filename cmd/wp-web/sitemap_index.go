package main

import (
	"context"
	"database/sql"
	"encoding/xml"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/elgris/sqrl"
	"github.com/golang/glog"
	"github.com/ssttevee/go-wordpress"
)

type W3CDatetime struct {
	time.Time
}

func (t W3CDatetime) String() string {
	// TODO: xml.Marshal is probably not using this
	return t.Format(time.RFC3339) // might be encoded with "Z" -- UTC -- rather than "+0000" which is in the old feed
}

type SitemapIndexSitemap struct {
	XMLName xml.Name     `xml:"sitemap"`
	Loc     string       `xml:"loc"`
	LastMod *W3CDatetime `xml:"lastmod,omitempty"`
}

type SitemapIndex struct {
	Sitemap []SitemapIndexSitemap
}

func NewSitemapIndex(wpctx context.Context, baseURL *url.URL) (*SitemapIndex, error) {
	si := &SitemapIndex{}

	si.Sitemap = append(si.Sitemap, SitemapIndexSitemap{Loc: fmt.Sprintf("%s/sitemap-misc.xml", baseURL)})
	si.Sitemap = append(si.Sitemap, SitemapIndexSitemap{Loc: fmt.Sprintf("%s/sitemap-tax-post_tag.xml", baseURL)})
	si.Sitemap = append(si.Sitemap, SitemapIndexSitemap{Loc: fmt.Sprintf("%s/sitemap-tax-category.xml", baseURL)})

	q := sqrl.Select()
	q = q.Column(sqrl.Alias(sqrl.Expr("MAX(post_modified_gmt)"), "last_changed"))
	q = q.Column(sqrl.Alias(sqrl.Expr("COUNT(id)"), "cnt"))
	q = q.Column(sqrl.Alias(sqrl.Expr("YEAR(wp_posts.post_date_gmt)"), "y"))
	q = q.Column(sqrl.Alias(sqrl.Expr("MONTH(wp_posts.post_date_gmt)"), "m"))
	q = q.Column("post_type")
	q = q.From(wordpress.Table(wpctx, "posts"))
	q = q.Where(sqrl.Eq{"post_type": []string{"post", "page"}})
	q = q.GroupBy("y", "m", "post_type")
	q = q.OrderBy("post_type DESC", "y DESC", "m DESC")
	db := wordpress.Database(wpctx)

	rows, err := q.RunWith(db).QueryContext(wpctx)
	if err != nil {
		q, args, err2 := q.ToSql()
		if err2 != nil {
			glog.Errorf("could not q.ToSql(): %v", err2)
		}

		glog.Errorf("failed running query %q (args %q): %q", q, fmt.Sprintf("%+v", args), err.Error())
		return nil, err // TODO: wrap err
	}
	for rows.Next() {
		var lastChanged sql.NullTime
		var cnt, y, m int
		var postType string
		if err := rows.Scan(&lastChanged, &cnt, &y, &m, &postType); err != nil {
			return nil, err // TODO: wrap err
		}
		// TODO: use lastChanged.Valid
		s := SitemapIndexSitemap{}
		if lastChanged.Valid {
			s.LastMod = &W3CDatetime{Time: lastChanged.Time}
		}
		s.Loc = fmt.Sprintf("%s/sitemap-pt-%s-%04d-%02d.xml", baseURL, postType, y, m)
		si.Sitemap = append(si.Sitemap, s)
	}

	si.Sitemap = append(si.Sitemap, SitemapIndexSitemap{Loc: fmt.Sprintf("%s/sitemap-authors.xml", baseURL)})
	
	return si, nil
}

func (web *WPWeb) SitemapIndex(wpctx context.Context, w http.ResponseWriter, r *http.Request) {
	index, err := NewSitemapIndex(wpctx, web.BaseURL)
	if err != nil {
		glog.Errorf("sitemapindex: %v", err)
		http.Error(w, "error generating sitemap index", http.StatusInternalServerError)
		return
	}
	enc := xml.NewEncoder(w)
	enc.Indent("", "\t")
	// TODO: handle encode errors
	enc.EncodeToken(xml.ProcInst{Target: "xml", Inst: []byte(`version="1.0" encoding="UTF-8"`)})
	enc.EncodeElement(index, xml.StartElement{Name: xml.Name{Local: "sitemapindex", Space: "http://www.sitemaps.org/schemas/sitemap/0.9"}})
}
