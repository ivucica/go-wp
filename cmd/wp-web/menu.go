package main

import (
	"context"
	"fmt"
	"os"

	"github.com/elliotchance/phpserialize"
	"github.com/ssttevee/go-wordpress"
)

type menu struct {
	wordpress.MenuItemList
	*wordpress.MenuLocation
}

type phpTheme struct {
	Theme string
}

// activePHPTheme returns the currently active theme and subtheme on the PHP
// installation, specified as options "template" and "stylesheet".
func (web *WPWeb) activePHPTheme(wpctx context.Context) (*phpTheme, *phpTheme, error) {
	theme, err := wordpress.GetOption(wpctx, "template")
	if err != nil {
		return nil, nil, fmt.Errorf("getoption(theme): %w", err)
	}
	subtheme, err := wordpress.GetOption(wpctx, "stylesheet")
	if err != nil {
		// no subtheme is fine.
		return &phpTheme{Theme: theme}, nil, nil
	}

	return &phpTheme{Theme: theme}, &phpTheme{Theme: subtheme}, nil
}

// options returns the options associated with the theme (or subtheme).
func (pt *phpTheme) options(wpctx context.Context) (map[string]interface{}, error) {
	return deserializedOption(wpctx, "theme_mods_" + pt.Theme)
}

// deserializedOption unpacks the option that is serialized using PHP's serialize().
func deserializedOption(wpctx context.Context, option string) (map[string]interface{}, error) {
	opt, err := wordpress.GetOption(wpctx, option)
	if err != nil {
		return nil, err
	}

	val := make(map[interface{}]interface{})
	if err := phpserialize.Unmarshal([]byte(opt), &val); err != nil {
		return nil, err
	}

	ret := make(map[string]interface{})
	for k, v := range val {
		if ks, ok := k.(string); ok {
			ret[ks] = v
		}
	}

	return ret, nil
}

// option returns the value associated with the specified option on a particular
// theme.
func (pt *phpTheme) option(wpctx context.Context, name string) (interface{}, error) {
	opts, err := pt.options(wpctx)
	if err != nil {
		return nil, err
	}

	if opt, ok := opts[name]; ok {
		return opt, nil
	}
	return nil, os.ErrNotExist
}

// navMenuLocation returns which menu is to be displayed at the specified named
// location (such as "primary").
func (pt *phpTheme) navMenuLocation(wpctx context.Context, location string) (int64, error) {
	opt, err := pt.option(wpctx, "nav_menu_locations")
	if err != nil {
		return 0, err
	}

	optMap, ok := opt.(map[interface{}]interface{})
	if !ok {
		return 0, fmt.Errorf("nav_menu_locations: wrong type")
	}

	if opt, ok := optMap[location]; ok {
		if locID, ok := opt.(int64); ok {
			return locID, nil
		} else {
			return 0, fmt.Errorf("wrong type %T", locID)
		}
	}
	return 0, fmt.Errorf("nav menu not specified for location %q", location)
}

// menuAtLocation returns the MenuLocation object (specifying menu's id, slug
// and name) and the menu items associated with whatever menu is used on the
// specified menu location in the current theme.
func (pt *phpTheme) menuAtLocation(wpctx context.Context, location string) (*menu, error) {
	menuID, err := pt.navMenuLocation(wpctx, location)
	if err != nil {
		return nil, err
	}

	var ml *wordpress.MenuLocation
	menus, err := wordpress.GetMenus(wpctx)
	if err != nil {
		return nil, err
	}

	for _, m := range menus {
		if m.Id == menuID {
			ml = m
			break
		}
	}

	if ml == nil {
		return nil, os.ErrNotExist
	}

	menuItems, err := wordpress.GetMenuItems(wpctx, &wordpress.ObjectQueryOptions{
		MenuId: menuID,
	})
	return &menu{
		MenuItemList: menuItems,
		MenuLocation: ml,
	}, err
}

func (web *WPWeb) menu(wpctx context.Context, locationName string) (*menu, error) {
	theme, subtheme, err := web.activePHPTheme(wpctx)
	if err != nil {
		return nil, err
	}
	menu, err := subtheme.menuAtLocation(wpctx, locationName)
	if err != nil {
		menu, err = theme.menuAtLocation(wpctx, locationName)
		if err != nil {
			return nil, err
		}
	}

	return menu, err
}
