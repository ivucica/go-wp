package main

import (
	"badc0de.net/pkg/flagutil"
	"flag"
	"fmt"
	"github.com/golang/glog"
	"github.com/ssttevee/go-wordpress"
	"net/http"
	"net/url"
)

var (
	dbHost = flag.String("db_host", "", "hostname")
	dbUser = flag.String("db_user", "", "username")
	dbPass = flag.String("db_pass", "", "password")
	dbName = flag.String("db_name", "", "database")
	dbPrfx = flag.String("db_prfx", "", "dbprefix")
	wpBase = flag.String("wp_base", "", "base url")
	wpCont = flag.String("wp_cont", "", "path to wp contents")
	siteID = flag.Int("site_id", -1, "site id")

	listenAddress = flag.String("listen_address", ":6743", "web server address")
	gaAccount     = flag.String("ga_account", "", "google analytics account id")
	robots        = flag.Bool("robots_allowed", false, "whether robots are allowed (changes what is served in robots.txt")
)

// Values set by linker flags.
var (
	BuildTimestamp = ""
)

func baseURL() *url.URL {
	u, err := url.Parse(*wpBase)
	if err != nil {
		glog.Fatalf("could not generate base url from %v: %v", *wpBase, err)
	}
	return u
}
func canonicalPostURL(post *wordpress.Post) *url.URL {
	if post == nil {
		return nil
	}
	switch wordpress.PostType(post.Type) {
	case wordpress.PostTypePost:
		u, err := url.Parse(fmt.Sprintf("%s/%s/%s/%s.html", *wpBase, post.Date.Format("2006"), post.Date.Format("01"), post.Name))
		if err != nil {
			glog.Warningf("failed to generate canonical url for post %d: %v", post.Id, err)
			return nil
		}
		return u
	case wordpress.PostTypePage:
		u, err := url.Parse(fmt.Sprintf("%s/%s", *wpBase, post.Name))
		if err != nil {
			glog.Warningf("failed to generate canonical url for post %d: %v", post.Id, err)
			return nil
		}
		return u
	default:
		glog.Errorf("canonicalPostType[%d]: post type %q unknown (%s)", post.Id, post.Type)
		return nil
	}
}
func ampPostURL(post *wordpress.Post) *url.URL {
	u := canonicalPostURL(post)
	if u == nil {
		return nil
	}
	u.RawPath += "/amp"
	u.Path += "/amp"
	return u
}

type logger struct {
	next http.Handler
}

func (l logger) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	ua := r.Header.Get("User-Agent")
	ra := r.Header.Get("X-Remote-Addr")
	ff := r.Header.Get("X-Forwarded-For")

	glog.Infof("Remote: %v; URI: %v; UA: %v; Remote Addr: %v, Forwarded For: %v, Form: %+v", r.RemoteAddr, r.RequestURI, ua, ff, ra, r.Form)
	l.next.ServeHTTP(w, r)
}

func main() {
	flagutil.Parse()
	glog.Infof("connecting")
	wp, err := wordpress.New(*dbHost, *dbUser, *dbPass, *dbName)
	if err != nil {
		glog.Fatalf("probl: %v", err)
	}
	defer wp.Close()
	wp.TablePrefix = *dbPrfx
	wp.SetMaxOpenConns(3)

	glog.Infof("starting to listen on %s, serving %q", *listenAddress, baseURL())

	web := WPWeb{
		db: wp,

		BaseURL:       baseURL(),
		PageSize:      6,
		WPContentPath: *wpCont,
		SiteID:        *siteID,
		GAAccount:     *gaAccount,
		RobotsAllowed: *robots,
	}
	web.tempfunc__Init()
	http.Handle("/", &logger{next: &web})
	glog.Errorf("done: %v", http.ListenAndServe(*listenAddress, nil))
}
