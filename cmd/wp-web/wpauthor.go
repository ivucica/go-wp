package main

import (
	"context"
	"fmt"

	"github.com/golang/glog"
	"github.com/ssttevee/go-wordpress"
)

type WPAuthor struct {
	AuthorID int64

	wpWeb   *WPWeb
	user    *wordpress.User
	context context.Context
}

func (a *WPAuthor) getUser() (*wordpress.User, error) {
	if a.user != nil {
		return a.user, nil
	}
	wpctx := a.context
	wpctx = wordpress.NewContext(wpctx, a.wpWeb.db)
	u, err := wordpress.GetUsers(wpctx, a.AuthorID) // FIXME(ivucica): This is erroring out: E1227 18:53:24.668395   26127 serve.go:247] wpauthor(1).name: Error 1055: Expression #4 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'DBNAMEHERE.um.meta_value' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by ; fix is to add um.meta_value to GroupBy.
	if err != nil {
		return nil, err
	}
	if len(u) == 0 {
		return nil, fmt.Errorf("author %d not found", a.AuthorID)
	}
	a.user = u[0]
	return a.user, nil
}

func (a *WPAuthor) Name() string {
	u, err := a.getUser()
	if err != nil {
		glog.Errorf("wpauthor(%d).name: %v", a.AuthorID, err)
		return fmt.Sprintf("author %d", a.AuthorID)
	}
	return u.Name
}

func (a *WPAuthor) Link() string {
	u, err := a.getUser()
	if err != nil {
		glog.Errorf("wpauthor(%d).link: %v", a.AuthorID, err)
		return fmt.Sprintf("%s/?author=%d", a.wpWeb.BaseURL, a.AuthorID)
	}
	return fmt.Sprintf("%s/author/%s", a.wpWeb.BaseURL, u.Slug)
}

func (a *WPAuthor) Email() string {
	u, err := a.getUser()
	if err != nil {
		glog.Errorf("wpauthor(%d).email: %v", a.AuthorID, err)
		return fmt.Sprintf("noreply@%v", a.wpWeb.BaseURL.Host)
	}
	return u.Email
}

func (a *WPAuthor) Gravatar() string {
	u, err := a.getUser()
	if err != nil {
		glog.Errorf("wpauthor(%d).gravatar: %v", a.AuthorID, err)
		return fmt.Sprintf("noreply@%v", a.wpWeb.BaseURL.Host)
	}
	return "https://www.gravatar.com/avatar/" + u.Gravatar + "?s=88&r=pg"
}
