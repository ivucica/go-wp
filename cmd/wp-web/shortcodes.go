package main

import (
	"context"
	"regexp"
	"strings"

	"github.com/golang/glog"

	// Used for ReplaceAllSubmatchFunc, otherwise equivalent to stdlib regexp
	// "github.com/agext/regexp"
	// Used because of (?! which isn't supported by stdlib regexp.
	"github.com/dlclark/regexp2"
)

type ShortcodeFunc func(ctx context.Context, attr map[string][]string, content []byte) ([]byte, error)
type ShortcodeFuncMap struct {
	re *regexp2.Regexp
	fm map[string]ShortcodeFunc
}

// RegisterShortcode places the specified shortcode callback function into a map
// keyed on the passed shortcode name.
func (web *WPWeb) RegisterShortcode(shortcode string, f ShortcodeFunc) {
	web.shortcodeFuncMap.RegisterShortcode(shortcode, f)
}

func (web *WPWeb) DoShortcode(wpctx context.Context, content []byte) ([]byte, error) {
	return web.shortcodeFuncMap.DoShortcode(wpctx, content)
}

// RegisterShortcode places the specified shortcode callback function into a map
// keyed on the passed shortcode name, and updates the precompiled search regex.
func (fm *ShortcodeFuncMap) RegisterShortcode(shortcode string, f ShortcodeFunc) {
	if fm.fm == nil {
		fm.fm = make(map[string]ShortcodeFunc)
	}
	fm.fm[shortcode] = f
	fm.re = fm.buildShortcodeRegexp()
}

// Shortcodes returns all keys from the associated funcmap.
func (fm *ShortcodeFuncMap) Shortcodes() (c []string) {
	for sc := range fm.fm {
		c = append(c, sc)
	}
	return
}

// QuoteMetaShortcodes returns all keys from the associated funcmap, but with
// QuoteMeta applied.
func (fm *ShortcodeFuncMap) QuoteMetaShortcodes() (c []string) {
	for sc := range fm.fm {
		c = append(c, regexp.QuoteMeta(sc))
	}
	return
}

// DoShortcode applies the registered shortcodes to the passed content.
//
// Does not perform do_shortcodes_in_html_tags. Does not support escaping tags
// using the [[tagname]] syntax. No parsing of attributes.
func (fm *ShortcodeFuncMap) DoShortcode(wpctx context.Context, content []byte) ([]byte, error) {
	if len(fm.fm) == 0 {
		// if funcmap is nil or empty, don't bother
		return content, nil
	}
	re := fm.buildShortcodeRegexp()

	// Not used because of (?! incompat.
	//
	// Switch to something based on this once (?! is eliminated.
	/*
		//r := re.ReplaceAllSubmatchFunc(content, func(m [][]byte) []byte {
			// Equivalent of do_shortcode_tag().
			return r[0]
		})
	*/

	r, err := re.ReplaceFunc(string(content), func(m regexp2.Match) string {
		tagName := m.GroupByNumber(2).String()
		// Allow [[tagname]].
		if (m.GroupByNumber(1).String() == "[" && m.GroupByNumber(6).String() == "]") ||
			(tagName[0] == '[' && tagName[len(tagName)-1] == ']') {
			return tagName[1 : len(tagName)-1]
		}
		if f, ok := fm.fm[tagName]; !ok {
			glog.Warningf("unhandled tag %s used", tagName)
			return m.GroupByNumber(0).String()
		} else {
			r, err := f(wpctx, nil, []byte(m.GroupByNumber(5).String()))
			if err != nil {
				glog.Errorf("executing function for shortcode %q failed: %v", tagName, err)
				return m.GroupByNumber(0).String()
			}
			return string(r)
		}
	}, -1, -1)

	return []byte(r), err
}

// buildShortcodeRegexpString builds a regexp search string based on the
// registered shortcodes' keys.
//
// Translated from GPL'd wp-includes/shortcodes.php and its get_shortcode_regex.
func (fm *ShortcodeFuncMap) buildShortcodeRegexpString() string {
	tagRegexp := strings.Join(fm.Shortcodes(), "|")
	return ("\\[" + // Opening bracket.
		"(\\[?)" + // 1: Optional second opening bracket for escaping shortcodes: [[tag]].
		"(" + tagRegexp + ")" + // 2: Shortcode name.
		"(?![\\w-])" + // Not followed by word character or hyphen.
		"(" + // 3: Unroll the loop: Inside the opening shortcode tag.
		"[^\\]\\/]*" + // Not a closing bracket or forward slash.
		"(?:" + //
		"\\/(?!\\])" + // A forward slash not followed by a closing bracket.
		"[^\\]\\/]*" + // Not a closing bracket or forward slash.
		")*?" + //
		")" + //
		"(?:" + //
		"(\\/)" + // 4: Self closing tag...
		"\\]" + // ...and closing bracket.
		"|" + //
		"\\]" + // Closing bracket.
		"(?:" + //
		"(" + // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags.
		//"[^\\[]*+" + // Not an opening bracket.
		"[^\\[]*" + // Not an opening bracket. // Removed possessive quantifier.
		"(?:" + //
		"\\[(?!\\/\\2\\])" + // An opening bracket not followed by the closing shortcode tag.
		//"[^\\[]*+" + // Not an opening bracket.
		"[^\\[]*" + // Not an opening bracket. // Removed possessive quantifier.
		//")*+" + //
		")*" + // Removed possesive quantifier.
		")" + //
		"\\[\\/\\2\\]" + // Closing shortcode tag.
		")?" + //
		")" + //
		"(\\]?)") // 6: Optional second closing brocket for escaping shortcodes: [[tag]].
}

// buildShortcodeRegexp compiles a regexp search string based on the registered
// shortcodes' keys.
func (fm *ShortcodeFuncMap) buildShortcodeRegexp() *regexp2.Regexp {
	return regexp2.MustCompile(fm.buildShortcodeRegexpString(), regexp2.None)
}
