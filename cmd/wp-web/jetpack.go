package main

import (
	"context"
	"net/http"

	"github.com/golang/glog"
)

func (web *WPWeb) jetpack(wpctx context.Context, w http.ResponseWriter, r *http.Request) bool {
	option, err := deserializedOption(wpctx, "jetpack-private-options")
	if err != nil {
		return false
	}
	glog.Errorf("%+v", option)
	return false
}

