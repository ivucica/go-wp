package main

import (
	"context"
	"fmt"

	"flag"
)

func init() {
	flag.Set("logtostderr", "true")
}

func ExampleWeb_DoShortcodes() {

	web := &WPWeb{}
	web.RegisterShortcode("world", func(ctx context.Context, attr map[string][]string, content []byte) ([]byte, error) {
		return []byte("earth"), nil
	})

	r, err := web.DoShortcode(context.TODO(), []byte("hello [world]"))
	if err != nil {
		panic(err)
	}
	fmt.Println(string(r))

	// Output:
	// hello earth
}
