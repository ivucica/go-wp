package main

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"io"
	"mime"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"

	"github.com/frustra/bbcode"
	"github.com/golang/glog"
	"github.com/microcosm-cc/bluemonday"
	"github.com/ssttevee/go-wordpress"
	"golang.org/x/net/html"
	"gopkg.in/russross/blackfriday.v2"
)

type WPPost struct {
	WPPost *wordpress.Post

	Title             string
	RenderableBody    template.HTML
	RenderableExcerpt template.HTML
	Author            *WPAuthor
	CreatedAt         time.Time

	IsMarkdown   bool
	CanonicalURL *url.URL
	AMPURL       *url.URL
	WPContentURL *url.URL

	Categories []*wordpress.Category
	Tags       []*wordpress.Tag
	Thumbnail  *wordpress.Attachment

	SiteBaseURL *url.URL

	Index int

	StatusCode  int
	UseFullBody bool

	// Visual
	Cols        int
	ColsDesktop int
}

func (p *WPPost) write(wpctx context.Context, w http.ResponseWriter, r *http.Request, web *WPWeb) {
	tmpl := web.templates
	if v := wpctx.Value("go-wp:serve-feed"); v != nil {
		tmpl = web.templatesFeed
	}
	if v := wpctx.Value("go-wp:serve-amp"); v != nil {
		tmpl = web.templatesAMP
	}

	if v := wpctx.Value("go-wp:single-post-mode"); v != nil {
		p.UseFullBody = true
	}

	if err := tmpl.ExecuteTemplate(w, "listing_entry_text_1", p); err != nil {
		glog.Errorf("wp.posts: err executing tmpl: %v", err)
		return
	}

	if p.Index == 0 {
		err := tmpl.ExecuteTemplate(w, "listing_about", struct {
			Cols        int
			ColsDesktop int
			BaseURL     *url.URL
		}{
			Cols:        12,
			ColsDesktop: 4,
			BaseURL:     web.BaseURL,
		})
		if err != nil {
			glog.Errorf("wp.posts: err executing tmpl: %v", err)
			return
		}
	}
}

// formatBBCode takes an HTML input string which may contain some BBCode and
// formats it into HTML with processed BBCode.
//
// Currently, this disables built-in tags, and processes only the contact-form
// tags into no output.
//
// Currently unused: we can't keep HTML unprocessed with this bbcode compiler.
func (web *WPWeb) formatBBCode(wpctx context.Context, in []byte) []byte {
	compiler := bbcode.NewCompiler(true, true) // autoCloseTags, ignoreUnmatchedClosingTags

	compiler.SetTag("b", nil)
	compiler.SetTag("url", nil)
	compiler.SetTag("img", nil)
	compiler.SetTag("center", nil)
	compiler.SetTag("color", nil)
	compiler.SetTag("size", nil)
	compiler.SetTag("quote", nil)
	compiler.SetTag("code", nil)

	compiler.SetTag("contact-form", func(node *bbcode.BBCodeNode) (*bbcode.HTMLTag, bool) {
		out := bbcode.NewHTMLTag("")
		out.Name = "div"
		out.Attrs["style"] = "display: none;"

		// "false" means don't parse child nodes.
		return out, false
	})
	compiler.SetTag("contact-field", func(node *bbcode.BBCodeNode) (*bbcode.HTMLTag, bool) {
		out := bbcode.NewHTMLTag("")
		out.Name = "div"
		out.Attrs["style"] = "display: none;"

		// "false" means don't parse child nodes.
		return out, false
	})
	return []byte(compiler.Compile(string(in)))
}

// formatPost takes a post fetched from the database and formats it.
//
// It also accesses the post's meta to understand if a post is markdown-formatted.
func (web *WPWeb) formatPost(wpctx context.Context, idx int, post *wordpress.Post) *WPPost {
	p := WPPost{
		WPPost:       post,
		Title:        post.Title,
		CanonicalURL: canonicalPostURL(post),
		AMPURL:       ampPostURL(post),
		WPContentURL: joinURL(web.BaseURL, "/wp-content"),
		Author:       &WPAuthor{AuthorID: post.AuthorId, wpWeb: web, context: wpctx},
		CreatedAt:    post.Date,
		Index:        idx,
		Categories:   []*wordpress.Category{},
		Tags:         []*wordpress.Tag{},
		SiteBaseURL:  web.BaseURL,
		StatusCode:   http.StatusOK,

		Cols:        12,
		ColsDesktop: 12,
	}

	if idx == 0 {
		p.ColsDesktop = 8
	}

	if post.Password != "" {
		glog.Infof("wpweb.posts: post %d is passworded", post.Id)
		p.RenderableExcerpt = template.HTML("<p>Post is passworded</p>")
		p.RenderableBody = template.HTML("<p>Post is passworded</p>")
		p.StatusCode = http.StatusUnauthorized
		return &p
	}

	categories, err := wordpress.GetCategories(wpctx, post.CategoryIds...)
	if err != nil {
		glog.Warningf("failed to get categories for post %d: %v", post.Id, err)
	} else {
		p.Categories = categories
	}

	tags, err := wordpress.GetTags(wpctx, post.TagIds...)
	if err != nil {
		glog.Warningf("failed to get tags for post %d: %v", post.Id, err)
	} else {
		p.Tags = tags
	}

	if post.FeaturedMediaId != 0 {
		if atts, err := wordpress.GetAttachments(wpctx, post.FeaturedMediaId); err != nil {
			glog.Errorf("failed to get attachment %d for post %d: %v", post.FeaturedMediaId, post.Id)
		} else {
			if len(atts) != 0 {
				att := atts[0]
				if typ, _, err := mime.ParseMediaType(att.MimeType); err == nil {
					if strings.HasPrefix(typ, "image/") {
						p.Thumbnail = att
					}
				}
			}
		}
	}

	var isMark string
	isMarkM, err := post.GetMeta(wpctx, "_cws_is_markdown") // TODO: use just post.Meta
	if err == nil {
		if isMarkS, ok := isMarkM["_cws_is_markdown"]; ok {
			isMark = isMarkS
		}
	}

	if isMark == "2" {
		p.IsMarkdown = true
	}

	p.RenderableExcerpt = template.HTML(post.Excerpt)
	p.RenderableBody += template.HTML(fmt.Sprintf("<!-- id: %d type: %v mime: %v is_markdown: %v -->\n", post.Id, post.Type, post.MimeType, isMark))

	const (
		DIRECT_CONTENT        = 0
		PARAGRAPHIZED_CONTENT = 1
		BLACK_FRIDAY          = 2
	)

	mode := PARAGRAPHIZED_CONTENT
	includeRawMarkdown := false
	if p.IsMarkdown {
		mode = BLACK_FRIDAY
	}

	// print rendered html
	switch mode {
	case DIRECT_CONTENT:
		p.RenderableBody += template.HTML(fmt.Sprintf("%s\n", post.Content))

	// Content is not necessarily already-rendered HTML.
	// TODO: Do anything else WP might do, such as ignore pre tags when adding <p>. See wpautop() for details.
	case PARAGRAPHIZED_CONTENT:
		input := strings.Replace(post.Content, "\r\n", "\n", -1)

		scB, err := web.DoShortcode(wpctx, []byte(input))
		if err != nil {
			glog.Errorf("shortcode execution error: %v", err)
			scB = []byte(post.Content)
		}

		z := html.NewTokenizer(bytes.NewReader(scB))
		pre := 0
		paragraphAccumulator := []byte{}
		dumpParagraphs := func() {
			if pre == 0 {
				paragraphs := strings.Split(string(paragraphAccumulator), "\n\n")
				for _, pg := range paragraphs {
					p.RenderableBody += template.HTML(fmt.Sprintf("<p>\n%s\n</p>\n\n", pg))
				}
			} else {
				p.RenderableBody += template.HTML(paragraphAccumulator)
			}
			paragraphAccumulator = []byte{}
		}
	loop:
		for {
			tt := z.Next()
			switch tt {
			case html.ErrorToken:
				// Returning io.EOF indicates success.
				if z.Err() != io.EOF {
					glog.Errorf("error tokenizing HTML: %v", z.Err())
				}
				dumpParagraphs()
				for i := 0; i < pre; i++ {
					p.RenderableBody += template.HTML("</pre>")
				}
				break loop
			case html.TextToken:
				paragraphAccumulator = append(paragraphAccumulator, z.Text()...)
			case html.StartTagToken:
				tn, _ := z.TagName()
				if string(tn) == "pre" {
					dumpParagraphs()
					pre++
					p.RenderableBody += template.HTML(z.Raw())
				} else {
					paragraphAccumulator = append(paragraphAccumulator, z.Raw()...)
				}
			case html.EndTagToken:
				tn, _ := z.TagName()
				if string(tn) == "pre" {
					dumpParagraphs()
					pre--
					p.RenderableBody += template.HTML(z.Raw())
				} else {
					paragraphAccumulator = append(paragraphAccumulator, z.Raw()...)
				}
			case html.SelfClosingTagToken, html.DoctypeToken, html.CommentToken:
				paragraphAccumulator = append(paragraphAccumulator, z.Raw()...)
			default:
				glog.Warningf("unknown html token type seen, breaking: %v", tt)
				break loop
			}

		}
	case BLACK_FRIDAY:

		// post.ContentFiltered contains raw markdown.
		//
		// It may be stored \r\n, which blackfriday doesn't seem to like.
		input := []byte(strings.Replace(post.ContentFiltered, "\r\n", "\n", -1))

		scB, err := web.DoShortcode(wpctx, input)
		if err != nil {
			glog.Errorf("shortcode execution error: %v", err)
			scB = input
		}
		input = scB

		unsafe := blackfriday.Run(input, blackfriday.WithExtensions(blackfriday.CommonExtensions|blackfriday.Footnotes))

		// Clean up for publication.
		policy := bluemonday.UGCPolicy()
		policy.AllowAttrs("class").Matching(regexp.MustCompile("^language-[a-zA-Z0-9]+$")).OnElements("code") // allow fenced code blocks. TODO(ivucica): can we also replace it with 'pre class="syntax go"' etc?
		policy.AllowAttrs("class").OnElements("pre")

		//html := string(policy.SanitizeBytes(unsafe))
		html := string(unsafe) // Let's trust the user for now. This would break <pre class="syntax"> and who knows what else.

		// Restore misprocessed comments.
		html = strings.Replace(html, "&lt;!&ndash;", "<!--", -1)
		html = strings.Replace(html, "&ndash;&gt;", "-->", -1)

		// Remove any restored comments. (Setting the s flag allows for . to match a newline.)
		commentsMatch := regexp.MustCompile("(?s)<!--.*-->")
		html = commentsMatch.ReplaceAllString(html, "")

		glog.V(2).Infof("post %d: INPUT: %s", post.Id, input)
		glog.V(2).Infof("post %d: OUTPUT: %s", post.Id, html)

		p.RenderableBody += template.HTML(html + "\n")
	}

	if includeRawMarkdown {
		p.RenderableBody += template.HTML(fmt.Sprintf("<textarea>%v</textarea>", post.ContentFiltered))
	}

	if len(p.RenderableExcerpt) == 0 {
		p.RenderableExcerpt = p.RenderableBody
	}

	return &p
}
