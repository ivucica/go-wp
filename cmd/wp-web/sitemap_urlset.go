package main

import (
	"context"
	"encoding/xml"
	"fmt"
	"net/http"
	"net/url"

	"github.com/golang/glog"
	"github.com/ssttevee/go-wordpress"
)

type SitemapPriority float32

func (sp SitemapPriority) String() string {
	// TODO: xml.Marshal might not be using this, we do want 1 to be encoded as 1.0
	return fmt.Sprintf("%.01f", sp)
}

type SitemapChangeFreq string

const (
	SitemapChangeFreqAlways  SitemapChangeFreq = "always"
	SitemapChangeFreqHourly  SitemapChangeFreq = "hourly"
	SitemapChangeFreqDaily   SitemapChangeFreq = "daily"
	SitemapChangeFreqWeekly  SitemapChangeFreq = "weekly"
	SitemapChangeFreqMonthly SitemapChangeFreq = "monthly"
	SitemapChangeFreqYearly  SitemapChangeFreq = "yearly"
	SitemapChangeFreqNever   SitemapChangeFreq = "never"
)

type SitemapURLSet struct {
	XMLName xml.Name     `xml:"urlset"` // xmlns: http://www.sitemaps.org/schemas/sitemap/0.9
	URL     []SitemapURL `xml:"url"`
}
type SitemapURL struct {
	Loc        string             `xml:"loc"`
	LastMod    *W3CDatetime       `xml:"lastmod,omitempty"`
	ChangeFreq *SitemapChangeFreq `xml:"changefreq,omitempty"`
	Priority   *SitemapPriority   `xml:"priority,omitempty"`
}

func NewSitemapURLSetForPostType(wpctx context.Context, baseURL *url.URL, pt wordpress.PostType, year, month int) (*SitemapURLSet, error) {
	us := SitemapURLSet{}
	it, err := wordpress.QueryPosts(wpctx, &wordpress.ObjectQueryOptions{
		Year:       year,
		Month:      month,
		PostType:   pt,
		PostStatus: wordpress.PostStatusPublish,
	})
	if err != nil {
		glog.Errorf("sitemap[%q, %d, %d] error: %v", pt, year, month, err)
		return nil, fmt.Errorf("error querying posts")
	}
	for it != nil {
		postIDs, err := it.Slice()
		if err != nil {
			glog.Errorf("sitemap[%q, %d, %d] error getting sitemap slice: %v", pt, year, month, err)
			return nil, fmt.Errorf("error getting sitemap slice")
		}
		posts, err := wordpress.GetPosts(wpctx, postIDs...)
		if err != nil {
			glog.Errorf("sitemap[%q, %d, %d] error getting posts: %v", pt, year, month, err)
			return nil, fmt.Errorf("error getting posts")
		}
		for _, post := range posts {
			// TODO: move this to "postToSitemapURL" reusable in other loops
			// or at least, "postSliceToSitemapURL"
			u := SitemapURL{}
			u.LastMod = &W3CDatetime{Time: post.ModifiedGmt}

			// TODO(ivucica): maybe we should not hardcode these two? e.g. calculate refresh rate based on history of revisions? last two revisions? or use a different constant if the revision is recent?
			switch pt {
			case wordpress.PostTypePost:
				cf := SitemapChangeFreqMonthly
				u.ChangeFreq = &cf
				p := SitemapPriority(0.2)
				u.Priority = &p
				u.Loc = fmt.Sprintf("%s/%d/%d/%s.html", baseURL, year, month, post.Name)
			case wordpress.PostTypePage:
				cf := SitemapChangeFreqWeekly
				u.ChangeFreq = &cf
				p := SitemapPriority(0.6)
				u.Priority = &p
				u.Loc = fmt.Sprintf("%s/%s", baseURL, post.Name)
			default:
				// We cover only those two, but eh
			}
			us.URL = append(us.URL, u)
		}

		num, err := it.Next()
		if err != nil {
			if err == wordpress.Done {
				break
			}
			glog.Infof("probl: %v", err)
			return nil, fmt.Errorf("error iterating")
		} else {
			glog.Infof("num: %d", num)
		}
	}
	return &us, nil
}

func (web *WPWeb) SitemapPostType(wpctx context.Context, w http.ResponseWriter, r *http.Request, pt wordpress.PostType, year, month int) {
	us, err := NewSitemapURLSetForPostType(wpctx, web.BaseURL, pt, year, month)
	if err != nil {
		glog.Errorf("sitemap[%q, %d, %d]: %v", pt, year, month, err)
		http.Error(w, "error generating sitemap", http.StatusInternalServerError)
		return
	}

	enc := xml.NewEncoder(w)
	enc.Indent("", "\t")
	// TODO: handle encode errors
	enc.EncodeToken(xml.ProcInst{Target: "xml", Inst: []byte(`version="1.0" encoding="UTF-8"`)})
	enc.EncodeElement(us, xml.StartElement{Name: xml.Name{Local: "urlset", Space: "http://www.sitemaps.org/schemas/sitemap/0.9"}})
}

func (web *WPWeb) SitemapMisc(wpctx context.Context, w http.ResponseWriter, r *http.Request) {

	// Last modified post = last modified homepage.
	lm := (*W3CDatetime)(nil)
	it, err := wordpress.QueryPosts(wpctx, &wordpress.ObjectQueryOptions{
		Order:          "post_modified_gmt",
		OrderAscending: false,
		Limit:          1,
		PostType:       wordpress.PostTypePost,
	})
	if err == nil {
		sl, err := it.Slice()
		if err == nil && len(sl) > 0 {
			p, err := wordpress.GetPosts(wpctx, sl...)
			if err == nil {
				lm = &W3CDatetime{Time: p[0].ModifiedGmt}
			}
		} else {
			glog.Error("error getting slice:", err)
		}
	} else {
		glog.Error("error querying posts:", err)
	}

	/////

	cf := SitemapChangeFreqDaily
	p := SitemapPriority(1.0)
	us := SitemapURLSet{
		URL: []SitemapURL{SitemapURL{Loc: fmt.Sprintf("%s/", web.BaseURL), ChangeFreq: &cf, Priority: &p, LastMod: lm}},
	}

	enc := xml.NewEncoder(w)
	enc.Indent("", "\t")
	// TODO: handle encode errors
	enc.EncodeToken(xml.ProcInst{Target: "xml", Inst: []byte(`version="1.0" encoding="UTF-8"`)})
	enc.EncodeElement(us, xml.StartElement{Name: xml.Name{Local: "urlset", Space: "http://www.sitemaps.org/schemas/sitemap/0.9"}})
}

func (web *WPWeb) SitemapAuthors(wpctx context.Context, w http.ResponseWriter, r *http.Request) {
	it, err := wordpress.QueryUsers(wpctx, &wordpress.UserQueryOptions{})
	if err != nil {
		glog.Errorf("could not query users: %v", err)
		http.Error(w, "could not query users", http.StatusInternalServerError)
		return
	}

	us := SitemapURLSet{}

	for it != nil {
		userIDs, err := it.Slice()
		if err != nil {
			glog.Errorf("could not get user slice: %v", err)
			http.Error(w, "could not get user slice", http.StatusInternalServerError)
			return
		}
		users, err := wordpress.GetUsers(wpctx, userIDs...)
		if err != nil {
			glog.Errorf("could not get users: %v", err)
			http.Error(w, "could not get users", http.StatusInternalServerError)
			return
		}

		for _, user := range users {
			glog.Infof("querying for user %v, id %d", user.Slug, user.Id)
			it, err := wordpress.QueryPosts(wpctx, &wordpress.ObjectQueryOptions{Author: user.Id, Limit: 1, Order: "post_modified_gmt", OrderAscending: false, PostType: wordpress.PostTypePost})
			if err == nil {
				sl, err := it.Slice()
				if err != nil {
					glog.Error("error getting slice:", err)
					continue
				}
				if len(sl) > 0 {
					p, err := wordpress.GetPosts(wpctx, sl...)
					if err == nil {
						lm := &W3CDatetime{Time: p[0].ModifiedGmt}
						cf := SitemapChangeFreqWeekly // are change freq and priority constant or calculated?
						p := SitemapPriority(0.3)
						us.URL = append(us.URL, SitemapURL{Loc: fmt.Sprintf("%s/author/%s", web.BaseURL, user.Slug), ChangeFreq: &cf, Priority: &p, LastMod: lm})
					}
				} else {
					// user has no posts
				}
			} else {
				glog.Error("error querying posts:", err)
			}
		}

		num, err := it.Next()
		if err != nil {
			if err == wordpress.Done {
				break
			}
			glog.Infof("probl: %v", err)
			return
		} else {
			glog.Infof("num: %d", num)
		}
	}

	enc := xml.NewEncoder(w)
	enc.Indent("", "\t")
	// TODO: handle encode errors
	enc.EncodeToken(xml.ProcInst{Target: "xml", Inst: []byte(`version="1.0" encoding="UTF-8"`)})
	enc.EncodeElement(us, xml.StartElement{Name: xml.Name{Local: "urlset", Space: "http://www.sitemaps.org/schemas/sitemap/0.9"}})
}

func (web *WPWeb) SitemapTaxonomy(wpctx context.Context, w http.ResponseWriter, r *http.Request, taxonomy string) {
	us := SitemapURLSet{}
	it, err := wordpress.QueryTerms(wpctx, &wordpress.TermQueryOptions{
		Taxonomy:       wordpress.Taxonomy(taxonomy),
		Order:          "slug",
		OrderAscending: true,
		Limit:          32767, // "everything" (rather than up to 10)
	})
	if err != nil {
		glog.Errorf("could not get terms for taxonomy %q: %v", taxonomy, err)
		http.Error(w, "could not get terms", http.StatusInternalServerError)
		return
	}

	cf := SitemapChangeFreqWeekly
	p := SitemapPriority(0.2)
	for it != nil {
		termIDs, err := it.Slice()
		if err != nil {
			glog.Errorf("could not get term slice: %v", err)
			http.Error(w, "could not get term slice", http.StatusInternalServerError)
			return
		}

		u := SitemapURL{}
		switch taxonomy {
		case "post_tag":
			tags, err := wordpress.GetTags(wpctx, termIDs...)
			if err != nil {
				glog.Errorf("could not get tags: %v", err)
				http.Error(w, "could not get tags", http.StatusInternalServerError)
				return
			}
			for _, tag := range tags {
				u.Loc = web.BaseURL.String() + tag.Link
				u.ChangeFreq = &cf
				u.Priority = &p
				postIt, err := wordpress.QueryPosts(wpctx, &wordpress.ObjectQueryOptions{TagId: tag.Id, Order: "post_modified_gmt", OrderAscending: false, Limit: 1, PostStatus: wordpress.PostStatusPublish})
				if err != nil {
					glog.Errorf("could not get latest post for tag %d: %v", tag.Id, err)
				} else {
					postIDs, err := postIt.Slice()
					if err != nil {
						glog.Errorf("could not get slice for latest post for tag %d: %v", tag.Id, err)
					} else {
						if len(postIDs) > 0 {
							posts, err := wordpress.GetPosts(wpctx, postIDs...)
							if err != nil {
								glog.Errorf("could not get post %d", postIDs)
							} else {
								if posts[0].Password == "" { // TODO(ivucica): Post querying should allow getting only non-passworded posts. If latest post in a tag is passworded, this will set lastmod to empty.
									u.LastMod = &W3CDatetime{Time: posts[0].ModifiedGmt}
								}
							}
						}
					}
				}
				us.URL = append(us.URL, u)
			}

		case "category":
			categories, err := wordpress.GetCategories(wpctx, termIDs...)
			if err != nil {
				glog.Errorf("could not get categories: %v", err)
				http.Error(w, "could not get categories", http.StatusInternalServerError)
				return
			}
			for _, category := range categories {
				u.Loc = web.BaseURL.String() + category.Link
				u.ChangeFreq = &cf
				u.Priority = &p
				postIt, err := wordpress.QueryPosts(wpctx, &wordpress.ObjectQueryOptions{Category: category.Id, Order: "post_modified_gmt", OrderAscending: false, Limit: 1, PostStatus: wordpress.PostStatusPublish})
				if err != nil {
					glog.Errorf("could not get latest post for category %d: %v", category.Id, err)
				} else {
					postIDs, err := postIt.Slice()
					if err != nil {
						glog.Errorf("could not get slice for latest post for category %d: %v", category.Id, err)
					} else {
						if len(postIDs) > 0 {
							posts, err := wordpress.GetPosts(wpctx, postIDs...)
							if err != nil {
								glog.Errorf("could not get post %d", postIDs)
							} else {
								if posts[0].Password == "" { // TODO(ivucica): Post querying should allow getting only non-passworded posts. If latest post in a category is passworded, this will set lastmod to empty.
									u.LastMod = &W3CDatetime{Time: posts[0].ModifiedGmt}
								}
							}
						}
					}
				}
				us.URL = append(us.URL, u)
			}
		}

		num, err := it.Next()
		if err != nil {
			if err == wordpress.Done {
				glog.Infof("done with all the %q!", taxonomy)
				break
			}
			glog.Infof("probl: %v", err)
			return
		} else {
			glog.Infof("num: %d", num)
		}
	}

	enc := xml.NewEncoder(w)
	enc.Indent("", "\t")
	// TODO: handle encode errors
	enc.EncodeToken(xml.ProcInst{Target: "xml", Inst: []byte(`version="1.0" encoding="UTF-8"`)})
	enc.EncodeElement(us, xml.StartElement{Name: xml.Name{Local: "urlset", Space: "http://www.sitemaps.org/schemas/sitemap/0.9"}})
}
